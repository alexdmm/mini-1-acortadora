#!/usr/bin/python3
from urllib.parse import unquote

import webapp
import shelve
import random
import string

dicc_urls = shelve.open('urlsRandomshort')  # Diccionario con clave recurso y valor url (guarda estado tras desconexion)


formulario = """
 <form action ="" method ="POST">
    Escriba la URL a acortar:<br>
    <input type="text" name="url"><br><br>
    <input type="submit" value="Enviar"><br>
 </form>
"""


class randomShortUrls(webapp.WebApp):
    def parse(self, request):
        if request == "":  # Para evitar el error cuando llega un solo elemento vacio
            return 'None'
        else:
            method, resource, _ = request.split(' ', 2)  # Trocea con espacio 2 veces
            body = request.split('\r\n\r\n', 1)[1]  # Trocea /r/n/r/n 1 vez y se queda con el cuerpo
            return method, resource, body

    def process(self, parsedRequest):
        if parsedRequest == 'None':  # Peticion despues de llegar al parse vacia (metodo no valido)
            method = parsedRequest
            print('\r\nMétodo: ' + method)
            http_code = "400 Bad Request"
            html_answer = ("<html><body><h3>Error al recibir la peticion.</h3></body></html>")
            return http_code, html_answer
        else:
            method, resource, body = parsedRequest
            print('\r\nMétodo: ' + method, '\r\nRecurso: ' + resource, '\r\nBody: ' + body)

            if method == "GET":
                if resource == "/":  # GET / para el recurso principal
                    lista = ""
                    for llave, valor in dicc_urls.items():
                        lista += "<li>" + "<a href=http://localhost:1234/" + str(llave) + ">" + \
                                 str(llave) + "</a>: " + valor + "</li>"
                    http_code = "200 OK"
                    html_answer = ("<html><body><h2>App para acortar URLs </h2><br>" +
                                   "<h3>" + formulario + "</h3><br>" +
                                   "Listado de URLs acortadas: " + "<ul>" +
                                   str(lista) + "</ul></body></html>")

                elif resource == "/favicon.ico":  # Contemplado recurso favicon.ico
                    http_code = "404 Not Found"
                    html_answer = ("<html><body><h3>Error. No se encuentra el recurso.</h3>" +
                                   "<a href=http://localhost:1234/>App para acortar URLs</a></body></html>")

                else:
                    recurso = str(resource.split("/")[1])  # Convertido a str para
                    # que sea del mismo tipo que la cadena aleatoria posterior del recurso acortado
                    if recurso in dicc_urls.keys():  # GET /recurso para URL previamente acortadas
                        print('REDIRECT: ' + str(dicc_urls[recurso]))
                        http_code = "302 Found"
                        html_answer = ("<html><head>" + "<meta http-equiv='refresh' content='3; URL=" +
                                       dicc_urls[recurso] + "'></head><body><h2> Redireccion a " +
                                       "<a href=" + dicc_urls[recurso] + ">" + str(dicc_urls[recurso]) +
                                       "</a></h2>" + "</body></html>")
                    else:  # Recursos que todavía no estan guardados
                        http_code = "404 Not Found"
                        html_answer = ("<html><body><h3>Error. No se encuentra el recurso.<br><br>" +
                                       "<a href=http://localhost:1234/>App para acortar URLs</a></h3></body></html>")
            else:  # If method == "POST"
                if body == "url=":  # Manejo del cuerpo vacio
                    http_code = "400 Bad Request"
                    html_answer = ("<html><body><h3>Error. Por favor, introduzca una URL en el formulario." +
                                   "<br><br><a href=http://localhost:1234/>App para acortar URLs" +
                                   "</a></h3></body></html>")
                else:
                    url_body = unquote(body.split("=")[1])  # Decodifica la URL para poder trabajar con ella
                    print(url_body)
                    if url_body.startswith(("https://www.", "http://www.")):
                        url_body = "http://" + url_body.split(".")[1] + "." + url_body.split(".")[2]
                    elif url_body.startswith(("https://", "http://")):
                        url_body = url_body
                    else:
                        url_body = "https://" + url_body

                    if url_body not in dicc_urls.values():
                        # El recurso va a ser una cadena alfanumerica aleatoria de 6 digitos
                        recursoURL = ''.join(random.choices(string.ascii_lowercase + string.digits, k=6))
                        if recursoURL in dicc_urls.keys():
                            recursoURL = ''.join(random.choices(string.ascii_lowercase + string.digits, k=6))

                        dicc_urls[recursoURL] = url_body
                    else:
                        pass

                    http_code = "200  OK"
                    lista = ""
                    for llave, valor in dicc_urls.items():
                        lista += "<li>" + "<a href=http://localhost:1234/" + str(llave) + ">" + \
                                 str(llave) + "</a>: " + valor + "</li>"
                    html_answer = ("<html><body><h2>App para acortar URLs </h2><br>" +
                                   "<h3>" + formulario + "</h3><br>" +
                                   "Listado de URLs acortadas: " + "<ul>" +
                                   str(lista) + "</ul></body></html>")
            return http_code, html_answer


if __name__ == "__main__":
    try:
        testWebApp = randomShortUrls("localhost", 1234)
    except KeyboardInterrupt:
        dicc_urls.close()
        print("Hasta pronto")
