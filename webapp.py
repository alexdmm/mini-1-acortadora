#!/usr/bin/python3
import socket


class WebApp:

    def parse(self, request):

        """Parseo la peticion extrayendo la info"""
        print("parese: No hay nada que  parsear")
        return None

    def process(self, parsed_request):
        """Procesa los datos de la peticion y va devolver el codigo HTTP de la respuesta y una pagina HTML"""
        print("procesar: No hay nada que procesar")
        return "200 OK", "<html><body>Hola mundo</body></html>"

    def __init__(self, hostname, port):
        """inicializa la aplicacion web"""

        # confifuaracion del socket
        mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mysocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mysocket.bind((hostname, port))
        mysocket.listen(5)

        while True:
            print("Esperando conexión...")
            conn, addr = mysocket.accept()
            print("conexion recibida de", str(addr))
            recibido = conn.recv(2048)
            print("La respuesta en bytes:", recibido)

            # Manejar/parsear peticion, los recursos
            parsed_request = self.parse(recibido.decode('utf-8'))

            # prorceso la peticion y creo la respuesta
            return_code, html_respuesta = self.process(parsed_request)

            # enviar respuesta
            respuesta = "HTTP/1.1 " + return_code + "\r\n\r\n" + html_respuesta + "\r\n"
            conn.send(respuesta.encode('utf-8'))
            conn.close()


if __name__ == "__main__":
    my_webapp = WebApp('', 1235)
